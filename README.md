Compile and deploy the contract:

```
truffle compile
truffle migrate
```

Start the dapp:

```
npm install
npm start
```

Dapp is server at `http://localhost:8080`.
