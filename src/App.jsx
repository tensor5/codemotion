import React, {Component} from 'react'
import {Col, Container, Row} from 'reactstrap'

import SendCoin from './SendCoin'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      balance: null,
      error: null
    }
  }

  async componentDidMount () {
    const {metacoin, web3} = this.props
    try {
      const instance = await metacoin.deployed()
      const accounts = web3.eth.accounts
      const balance = await instance.getBalance.call(accounts[0])
      this.setState({
        balance
      })
    } catch (e) {
      this.setState({
        balance: null,
        error: `Error: ${e.message}`
      })
    }
  }

  render () {
    const {balance, error} = this.state
    return (
      <Container>
        <Row>
          <Col>
            <h1 className='text-center'>
              {balance != null
                ? `Your balance is ${balance.toFormat()}`
                : error != null
                  ? error
                  : 'Loading...'
              }
            </h1>
          </Col>
        </Row>
        <Row>
          <Col>
            <SendCoin metacoin={this.props.metacoin} web3={this.props.web3} />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default App
