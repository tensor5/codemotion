import React, {Component} from 'react'
import {Button, Form, FormGroup, Input, Label} from 'reactstrap'

class SendCoin extends Component {
  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
  }

  async onSubmit (e) {
    e.preventDefault()
    const address = e.target.address.value
    const amount = e.target.amount.value
    if (address !== '' && amount !== '') {
      const instance = await this.props.metacoin.deployed()
      const accounts = this.props.web3.eth.accounts
      instance.sendCoin(address, amount, {from: accounts[0]})
    }
  }

  render () {
    return (
      <Form onSubmit={this.onSubmit}>
        <FormGroup>
          <Label>Receiver</Label>
          <Input type='text' name='address' placeholder='0x...' />
        </FormGroup>
        <FormGroup>
          <Label>Amount</Label>
          <Input type='text' name='amount' />
        </FormGroup>
        <Button block type='submit'>Send</Button>
      </Form>
    )
  }
}

export default SendCoin
