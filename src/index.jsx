import React from 'react'
import ReactDOM from 'react-dom'
import truffleContract from 'truffle-contract'
import Web3 from 'web3'

import MetaCoinJson from '../build/contracts/MetaCoin.json'
import './styles'
import App from './App'

const MetaCoin = truffleContract(MetaCoinJson)
const web3 = new Web3()

const app = document.getElementById('app')

window.addEventListener('load', () => {
  let provider

  if (window.web3) {
    provider = window.web3.currentProvider
  } else {
    console.log('No web3? You should consider trying MetaMask!')
    provider = new Web3.providers.HttpProvider('http://localhost:8545')
  }

  web3.setProvider(provider)
  MetaCoin.setProvider(provider)
  ReactDOM.render(<App metacoin={MetaCoin} web3={web3} />, app)
})
